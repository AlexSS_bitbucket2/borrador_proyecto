//carga los valores con la configuración por defecto
require('dotenv').config();

//console.log("Hola mundo");
const express = require('express');
const app = express();

//define el puerto por el que escuchar, y sin lo asigna al 3000
const port = process.env.PORT || 3000;

var enableCORS = function(req, res, next) {
 res.set("Access-Control-Allow-Origin", "*");
 res.set("Access-Control-Allow-Methods", "POST, GET, OPTIONS, DELETE, PUT");

 res.set("Access-Control-Allow-Headers", "Content-Type");
 next();
}

//para parsear el body de los textos sin tener que hacerlo nosotros, y espera que sea json o casca.
app.use(express.json());
app.use(enableCORS);

//Definir variables para utilizar las funciones del controller
const userController = require('./controllers/UserController');
const authController = require('./controllers/AuthController');
const cuentaController = require('./controllers/CuentaController');

app.listen(port);
console.log("API escuchando en el puerto:" + port);

app.get('/apitechu/v1/users', userController.getUsersV1);
app.get('/apitechu/v2/users', userController.getUsersV2);
app.get('/apitechu/v2/users/:id', userController.getUserByIdV2);
app.post('/apitechu/v1/users', userController.postUsersV1);
app.post('/apitechu/v2/users', userController.createUserV2);
app.delete('/apitechu/v1/users/:id',userController.deleteUsersV1);

app.post('/apitechu/v1/login', authController.postLoginV1);
app.post('/apitechu/v2/login', authController.postLoginV2);
app.post('/apitechu/v1/logout/:id', authController.postLogoutV1);
app.post('/apitechu/v2/logout/:id', authController.postLogoutV2);

app.get('/apitechu/v1/cuentas', cuentaController.getCuentasV1);
app.get('/apitechu/v1/cuentas/:IdUsuario', cuentaController.getCuentasByIdV1);

app.get('/apitechu/v1/hello',
  function(req, res) {
    console.log("GET /apitechu/v1/hello");
    res.send({"msg" : "Hola desde el API GET"});
  }
)

app.post("/apitechu/v1/monstruo/:p1/:p2",
  function (req, res) {
    console.log ("Parámetros");
    console.log (req.params);

    console.log("Query String");
    console.log(req.query);

    console.log("Headers");
    console.log(req.headers);

    console.log("Body");
    console.log(req.body);
  }
)
