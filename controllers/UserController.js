const io = require('../io');
const requestJson = require('request-json');
const baseMLabURL = "https://api.mlab.com/api/1/databases/apitechuass10ed/collections/";
const mLabAPIKey = "apiKey=" + process.env.MLAB_APY_KEY;
const crypt = require('../crypt');

function getUsersV1 (req, res) {
  console.log("GET /apitechu/v1/users");
  //res.sendFile('usuarios.json', {root: __dirname});

  var respuesta = {};
  var users = require('../usuarios.json');

  //filtro de $count
  if (req.query.$count == 'true') {
    console.log("filtro $count = true");
    respuesta.count = users.length;
    console.log("nº elementos del fichero: " + respuesta);
  }

  //filtro de $top
  if (req.query.$top) {
    console.log("filtro $top > 0");
    respuesta.users = users.slice(0,req.query.$top);
    console.log("seleccionados: " + respuesta);
  }
  else {
    respuesta.users = users;
  }
 res.send(respuesta);
}

function getUsersV2 (req, res) {
  console.log("GET /apitechu/v2/users");

  var httpClient = requestJson.createClient(baseMLabURL);
  console.log("Client created");

  httpClient.get("user?" + mLabAPIKey,
    function(err, resMLab, body){
      var response = !err ? body : {
        "msg" : "Error obteniendo usuarios"
      }

      res.send(response);
    }
  )
}

function getUserByIdV2(req, res) {
  console.log("GET /apitechu/v2/users/:id");

  var httpClient = requestJson.createClient(baseMLabURL);
  console.log("Client created");
  console.log("ID = " + req.params.id);

  var query = "q={'id':" + req.params.id + "}";

  console.log("la query es = " + query);

  httpClient.get("user?" + query + "&" + mLabAPIKey,
    function(err, resMLab, body) {
      if (err) {
        var response = {
          "msg" : "Error obteniendo usuario"
        }
        res.status(500);
      } else {
        if (body.length > 0) {
          var response = body[0];
        } else {
          var response = {
            "msg" : "Usuario no encontrado"
          }
          res.status(404);
        }
      }
      res.send(response);
    }
  )
}
//    function(err, resMLab, body){
//      var response = !err ? body[0] : {
//        "msg" : "Error obteniendo el usuario"
//      }
//    }
//      res.send(response);


function createUserV2(req, res) {
  console.log("POST /apitechu/V2/users");

  console.log(req.body.id);
  console.log(req.body.first_name);
  console.log(req.body.last_name);
  console.log(req.body.email);
  console.log(req.body.password);


  var newUser = {
    "id" : req.body.id,
    "first_name": req.body.first_name,
    "last_name": req.body.last_name,
    "email": req.body.email,
    "password" : crypt.hash(req.body.password)
  }

  var httpClient = requestJson.createClient(baseMLabURL);
  console.log("Client created");

  httpClient.post("user?" + mLabAPIKey, newUser,
    function(err, resMLab, body){
      console.log("Usuario creado en MLab");
//        res.send(response);
      res.status(201).send({"msg":"Usuario creado"});
    }
  )
}


function postUsersV1(req, res) {
  console.log("POST /apitechu/v1/users");

  //console.log(req.headers);
  //console.log(req.headers.first_name);
  //console.log(req.headers.last_name);
  //console.log(req.headers.email);

  //var newUser = {
  //  "first_name": req.headers.first_name,
  //  "last_name": req.headers.last_name,
  //  "email": req.headers.email
  //}

  //console.log(req.body);
  console.log(req.body.first_name);
  console.log(req.body.last_name);
  console.log(req.body.email);

  var newUser = {
    "first_name": req.body.first_name,
    "last_name": req.body.last_name,
    "email": req.body.email
  }

  //user que es un array en memoria
  var users = require('../usuarios.json');
  users.push(newUser);
  io.writeUserDatatoFile(users);
  console.log("Usuario añadido con éxito");

  res.send({"msg": "Usuario añadido con exito"});
}

function deleteUsersV1(req, res) {
  console.log("DELETE /apitechu/v1/users/:id");
  console.log("id es:" + req.params.id);

  var users = require('../usuarios.json');

//alternativa 1: for
  for (var i = 0; i < users.length; i++) {
    if (users[i].id == req.params.id)  {
      console.log("encontrado i==!!!" + i);
      users.splice(i, 1);
    }
  }
  io.writeUserDatatoFile(users);
  console.log("Usuario borrado con éxito");
  res.send({"msg": "Usuario borrado con exito"});
}

//referenciar modulos para exponer la función afuera
module.exports.getUsersV1 = getUsersV1;
module.exports.getUsersV2 = getUsersV2;
module.exports.getUserByIdV2 = getUserByIdV2;
module.exports.postUsersV1 = postUsersV1;
module.exports.createUserV2 = createUserV2;
module.exports.deleteUsersV1 = deleteUsersV1;
