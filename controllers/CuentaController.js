const io = require('../io');
const requestJson = require('request-json');
const baseMLabURL = "https://api.mlab.com/api/1/databases/apitechuass10ed/collections/";
const mLabAPIKey = "apiKey=" + process.env.MLAB_APY_KEY;
const crypt = require('../crypt');

function getCuentasV1 (req, res) {
  console.log("GET /apitechu/v1/cuentas");

  var httpClient = requestJson.createClient(baseMLabURL);
  console.log("Cuenta creada");

  httpClient.get("cuentas?" + mLabAPIKey,
    function(err, resMLab, body){
      var response = !err ? body : {
        "msg" : "Error obteniendo cuentas"
      }

      res.send(response);
    }
  )
}

function getCuentasByIdV1(req, res) {
  console.log("GET /apitechu/v1/cuentas/:IdUsuario");
  console.log("IdUsuario = " + req.params.IdUsuario);

  // el IdUsuario es el de la colección
  var query = "q={'IdUsuario':" + req.params.IdUsuario + "}";

  console.log("la query es = " + query);

  var httpClient = requestJson.createClient(baseMLabURL);

  httpClient.get("cuentas?" + query + "&" + mLabAPIKey,
    function(err, resMLab, body) {
      if (err) {
        var response = {
          "msg" : "Error obteniendo cuentas de usuario"
        }
        res.status(500);
      } else {
        if (body.length > 0) {
          var response = body;
        } else {
          var response = {
            "msg" : "Cuentas de Usuario no encontrado"
          }
          res.status(404);
        }
      }
      res.send(response);
    }
  )
}

//referenciar modulos para exponer la función afuera
module.exports.getCuentasV1 = getCuentasV1;
module.exports.getCuentasByIdV1 = getCuentasByIdV1;
