const io = require('../io');
const requestJson = require('request-json');
const crypt = require('../crypt');
const baseMLabURL = "https://api.mlab.com/api/1/databases/apitechuass10ed/collections/";
const mLabAPIKey = "apiKey=" + process.env.MLAB_APY_KEY;

function postLoginV1(req, res) {
  console.log("POST /apitechu/v1/login");

  console.log(req.body.email);
  console.log(req.body.password);

  var users = require('../auth.json');
  var encontrado = false;

  //buscamos con FOR para localizar el email y password del usuario
  for (var i = 0; i < users.length; i++) {
    if ((users[i].email == req.body.email) && (users[i].password == req.body.password))   {
      console.log("encontrado i==!!!" + i);
      encontrado = true;
      users[i].logged = true;
      io.writeUserDatatoFileAuth(users);
//      res.send({"msg": "Login correcto. ID usuario = " + users[i].id});
        res.send({"msg":"Login correcto.", "ID usuario": + users[i].id});
      break;
    }
  }

  if (encontrado == false) {
      res.send({"msg": "Login incorrecto"});
  }
}

function postLoginV2(req, res) {
  console.log("POST /apitechu/v2/login");

  console.log(req.body.email);
  console.log(req.body.password);

  var query = 'q={"email": "' + req.body.email + '"}';
  console.log("la query es = " + query);

  var putBody = '{"$set":{"logged":true}}';
  var httpClient = requestJson.createClient(baseMLabURL);

  httpClient.get("user?" + query + "&" + mLabAPIKey,
    function(errgetlogin, resMLabgetlogin, bodygetlogin) {
      console.log("dentro función get");
      console.log(bodygetlogin);
      console.log(bodygetlogin[0].id);
      console.log(req.body.email);

      console.log(bodygetlogin[0].password);
      console.log(req.body.password);

      if (crypt.checkPassword(req.body.password, bodygetlogin[0].password)) {
        httpClient.put("user?" + query + "&" + mLabAPIKey,JSON.parse(putBody),
          function(errputlogin, resMLabputlogin, bodyputlogin) {
            if (errputlogin) {
              var response =
              {
                "msg" : "Error actualizando usuario"
              }
              res.send(response);
            }
             else
             {
                console.log("Usuario logado");
                res.send({"msg": "Usuario logado.", "id" : bodygetlogin[0].id});
                console.log(bodygetlogin[0].id);
            }
          }
        )
      }
    }
  )
}


function postLogoutV1(req, res) {
  console.log("POST /apitechu/v1/logout");

  var users = require('../auth.json');
  var encontrado = false;

  //buscamos con FOR para localizar el ID de usuario
  for (var i = 0; i < users.length; i++) {
    if ((users[i].id == req.params.id) && (users[i].logged))   {
      console.log("encontrado i==!!! con logged = true" + i);
      encontrado = true;
      delete users[i].logged
      io.writeUserDatatoFileAuth(users);
      res.send({"msg": "Deslogado.", "ID Usuario" : users[i].id});
      break;
    }
  }
//  if (encontrado == false) {
//      res.send({"msg": "Login incorrecto"});
//  }
}

function postLogoutV2(req, res) {
  console.log("POST /apitechu/v2/logout");

  var httpClient = requestJson.createClient(baseMLabURL);

  var query = 'q={"id": ' + req.params.id + '}';

  console.log("la query es = " + query);

  httpClient.get("user?" + query + "&" + mLabAPIKey,
    function(errgetlogout, resMLabgetlogout, bodygetlogout) {
      var putBody = '{"$unset":{"logged":""}}'

      httpClient.put("user?" + query + "&" + mLabAPIKey, JSON.parse(putBody),
        function(errputlogout, resMLabputlogout, bodyputlogout) {
          console.log("deslogeado usuario");
          console.log(bodygetlogout);
          res.send({"msg": "Deslogado.", "ID Usuario" : bodygetlogout[0].id});
        }
      )
    }
  )
}

module.exports.postLoginV1 = postLoginV1;
module.exports.postLoginV2 = postLoginV2;
module.exports.postLogoutV1 = postLogoutV1;
module.exports.postLogoutV2 = postLogoutV2;
