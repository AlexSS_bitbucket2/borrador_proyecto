const fs = require('fs');

//función para escribir en el fichero
function writeUserDatatoFile(data){
  console.log("writeUserDatatoFile");

  //convertir a JSON el users que es un array
  var jsonUserData = JSON.stringify(data);

  //escribir en el fichero
  fs.writeFile("./usuarios.json", jsonUserData, "utf8",
    function(err) {
        if(err) {
          console.log(err);
        } else {
          console.log("Usuario persistido");
        }
    }
  )
}

//función para escribir en el fichero
function writeUserDatatoFileAuth(data){
  console.log("writeUserDatatoFileAuth");

  //convertir a JSON el users que es un array
  var jsonUserData = JSON.stringify(data);

  //escribir en el fichero
  fs.writeFile("./auth.json", jsonUserData, "utf8",
    function(err) {
        if(err) {
          console.log(err);
        } else {
          console.log("Usuario persistido en fichero Auth");
        }
    }
  )
}

module.exports.writeUserDatatoFile = writeUserDatatoFile;
module.exports.writeUserDatatoFileAuth = writeUserDatatoFileAuth;
